#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Main

Injest a csv of PTV records. Categorise them and return predictions for
duration, speed and delay based on those categories. Predictions are
based on prepared data.
"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.6"
__date__ = "2018-12-28"


if __name__ == '__main__':
    """Example usage"""
    import os
    from functools import partial

    from predictor import ComboPredictor
    from process_input import generate_input_rows, categorise
    from create_output import OutputClient

    # Sample file containing records for predictions
    INPUT_FILENAME = os.path.join("data", "ptv_events_data_sample.csv")

    """ Using the ComboPredictor """

    PTV_TRAINING_FILENAME = os.path.join("data", "incidents_stats.csv")
    LOOP_TRAINING_FILENAME = os.path.join("data", "italy_stats.csv")

    predictor = ComboPredictor(PTV_TRAINING_FILENAME, LOOP_TRAINING_FILENAME)
    output = OutputClient(filename="output.csv")

    # Categorise the data
    categorised_data = map(partial(categorise, predictor.categories),
                           generate_input_rows(INPUT_FILENAME))
    # Generate predictions
    predictions = map(predictor.predict, categorised_data)

    # Create output file
    output.write(predictions)
