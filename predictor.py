#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Predictor

A predictive model, train it or load it, and make a prediction.
"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.6"
__date__ = "2018-12-28"

import pandas as pd

MAXIMUM_MINUTES = 240


def cap_duration_hours(df):
    """Filter dataframe to remove rows that are not within
    the acceptable duration"""

    def within_acceptable_duration(duration_hours):
        """Test a duration_hour for acceptance (True) or exclusion (False)"""
        return MAXIMUM_MINUTES >= duration_hours * 60  # convert to minutes

    return df[within_acceptable_duration(df.duration_hours)]


class Predictor:
    """Base Class for Predictors

    Applies a predictive model to make a prediction about the traffic.
    Provides predifined OUTPUT_FIELDS and promises the predict method.
    """

    # Ensure consistent output fields
    OUTPUT_FIELDS = ["date_time",
                     "x", "y", "z",
                     "segment",
                     "length_m",
                     "length_s",
                     "street_category",
                     "predicted_duration",
                     "predicted_delay",
                     "predicted_speed",
                     "incident_message"
                     ]

    def predict(*args, **kwargs):
        raise NotImplemented("A Predictor must implement a predict method")


class PTVPredictor(Predictor):
    """PTV data predictions"""

    def __init__(self, filename: str=None, categories: str=None):
        """Initialise the predictive model to use"""
        if categories is not None:
            self.categories = categories
        else:
            raise RuntimeError("Categories required for training")
        if filename is not None:
            # train the predictive model
            self.train(filename)
        else:
            raise RuntimeError("Filename required for training")

    def train(self, filename: str):
        """ Create the dataframe with categories """
        df = pd.read_csv(filename)
        for cat in self.categories:
            df[cat] = df.message.str.contains(cat)
        self.df = cap_duration_hours(df)

    def get_category(self, category: str):
        """ Get all dataframe rows for a category """
        return self.df[self.df[category]]

    def get_segment_category(self, segment: str, category: str):
        """ Get all dataframe rows for a category and segment """
        cat = self.get_category(category)
        return cat[cat.segment_id == segment]

    def predict_attr(self, attr: str, record: dict) -> float:
        """ Make a prediction for a given attribute

        First attempt a prediction based on segment and category information
        Fall back on a prediction based on only category information
        Fall back on a prediction based on all data

        Args:
            attr: attribute, must be a column heading from the dataframe
            record: a PTV data dict

        Returns:
            the mean value for the attribute of the available, suitable rows
        """
        r_seg = int(float(record['segment']))
        r_cat = record['category']
        if r_seg is not None and r_cat is not None:
            seg_cat = self.get_segment_category(r_seg, r_cat)
            if len(seg_cat) > 0:
                return seg_cat[attr].mean()
        if r_cat is not None:
            cat = self.get_category(r_cat)
            if len(cat) > 0:
                return cat[attr].mean()
        return self.df[attr].mean()

    def predict(self, record: dict) -> dict:
        """Given a PTV record, add predictions
        and return with defined output fields

        Args:
            record: a PTV record as a dict

        Returns:
            record with additional predictions, keys are predefined
        """
        record["predicted_duration"] = self.predict_attr("duration_hours",
                                                         record)
        record["predicted_speed"] = self.predict_attr("absolute_speed_min",
                                                      record)
        record["predicted_delay"] = self.predict_attr("delay_max", record)
        return {o: record[o] for o in self.OUTPUT_FIELDS}


class LoopPredictor(Predictor):
    """ Loop data predictions """

    def __init__(self, filename: str=None):
        """ Load in the csv to a dataframe """
        if filename is not None:
            # train the predictive model
            self.df = pd.read_csv(filename)
        else:
            raise RuntimeError("Filename required for training")

    @property
    def categories(self):
        """ Incident categories are extracted from the CSV """
        return self.df.incident_type

    def predict_attr(self, attr: str, record: dict) -> float:
        """ Make a prediction for the given attribute

        Args:
            attr: attribute, must be a column heading from the dataframe
            record: a PTV data dict

        Returns:
            the mean value, looked up in the CSV file
        """

        return self.df[self.df.incident_type == record['category']].iloc[0][attr]

    def predict(self, record: dict) -> dict:
        """Given a PTV record, add predictions
        and return with defined output fields

        Args:
            record: a PTV record as a dict

        Returns:
            record with additional predictions, keys are predefined
        """
        record["predicted_duration"] = "NA"
        record["predicted_speed"] = self.predict_attr("speed_mean",
                                                      record)
        record["predicted_delay"] = self.predict_attr("delay_mean", record)
        return {o: record[o] for o in self.OUTPUT_FIELDS}


class ComboPredictor(Predictor):
    """Combo Predictor using PTV data and Loop data"""

    def __init__(self, ptv_filename: str=None, loop_filename: str=None):
        self._loop = LoopPredictor(loop_filename)
        self._ptv = PTVPredictor(ptv_filename, self._loop.categories)

    @property
    def categories(self):
        return self._loop.categories

    def predict(self, record: dict) -> dict:
        """Given a PTV record, add predictions
        and return with defined output fields.

        Uses Loop Predictor for speed and delay, PTV Predictor
        for duration.

        Args:
            record: a PTV record as a dict

        Returns:
            record with additional predictions, keys are predefined
        """
        record["predicted_duration"] = self._ptv.predict_attr("duration_hours",
                                                              record)
        record["predicted_speed"] = self._loop.predict_attr("speed_mean",
                                                            record)
        record["predicted_delay"] = self._loop.predict_attr("delay_mean",
                                                            record)
        return {o: record[o] for o in self.OUTPUT_FIELDS}
