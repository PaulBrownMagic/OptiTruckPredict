create\_output module
=====================

.. automodule:: create_output

.. autoclass:: OutputClient
    :members:
    :undoc-members:
    :show-inheritance:
