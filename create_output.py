#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Write out processed data"""

__author__ = "Paul Brown"
__contact__ = "pbrownpaul@gmail.com"
__version__ = "0.0.2"
__date__ = "2018-12-19"

from csv import DictWriter
from functools import partial
from json import dumps, dump


def csv_first_row(rows) -> (dict, (dict)):
    """ Split the first row from the rows

    Polymorphic to handle cases where rows is a list or a generator

    Args:
        rows: generator or list of record dicts
    """
    if type(rows) == list:
        return rows[0], rows[1:]
    else:
        r = next(rows)
        return r, rows


def csv_stdout(rows: (dict)) -> None:
    """Output the data as csv to stdout

    Args:
        rows: record dicts
    """
    def csv(iterable):
        """Output one row in csv format"""
        print(", ".join(map(str, iterable)))

    head, tail = csv_first_row(rows)

    # Header row
    csv(head.keys())
    csv(head.values())

    # Data rows
    for row in tail:
        csv(row.values())


def csv_file(rows: (dict), filename: str) -> None:
    """Output the data as csv to filename

    Args:
        rows: record dicts
    """

    head, tail = csv_first_row(rows)

    with open(filename, 'w') as csvfile:
        fieldnames = head.keys()  # initial order retained
        writer = DictWriter(csvfile, fieldnames=fieldnames)

        # Header row
        writer.writeheader()

        # Data rows
        writer.writerow(head)
        for row in tail:
            writer.writerow(row)


def json_stdout(rows: (dict)) -> None:
    """Output the data as json to stdout

    Args:
        rows: record dicts
    """
    # default=str to serialise custom types and print their original strings
    print(dumps(list(rows), default=str, indent=2))


def json_file(rows: (dict), filename: str) -> None:
    """Output the data as json to filename

    Args:
        rows: record dicts
     """
    with open(filename, 'w') as jsonfile:
        # default=str to serialise custom types and print original strings
        dump(list(rows), jsonfile, default=str, indent=2)


class OutputClient:
    """Interface to choose the output format and destination of the data

    Options:
        output_format: "csv" or "json"
        filename: None or path to output file

    Note: JSON methods force rows to be evaluated into a list before writing
    out to file, whereas CSV can use generators throughout.

    If filename is None (the default), output will be printed to stdout.
    """
    _formats = {("csv", "stdout"): csv_stdout,
               ("csv", "file"): csv_file,
               ("json", "stdout"): json_stdout,
               ("json", "file"): json_file
               }

    def __init__(self, output_format: str="csv", filename: str=None) -> None:
        """Set the format and destination"""
        if filename is None:
            self.writer = self._formats[(output_format, "stdout")]
        else:
            self.writer = partial(self._formats[(output_format, "file")],
                                  filename=filename)

    def write(self, rows: (dict)) -> None:
        """Write out the data"""
        self.writer(rows=rows)
