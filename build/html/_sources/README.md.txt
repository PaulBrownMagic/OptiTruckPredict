# OptiTruck Predict

[Documentation Home](https://paulbrownmagic.gitlab.io/OptiTruckPredict/)

Provides predictions for delay, duration and speed based on a PTV
record. Accepts CSV file of records as an input and will output CSV file
with predictions and *some* of the original PTV record.

## Dependencies

Required Python libraries are handled using the Python Foundation recommended
[pipenv](https://pipenv.readthedocs.io/en/latest/), they are listed in `Pipfile` and can be installed with
`pipenv install`. For more information, see the docs.

A Python version has not been dictated for the wider project this is part of. Therefore this has been developed using
the latest stable release (Python 3.7), but should work also work with Python 3.6. Earlier versions will not work
due to the use of F-strings, which were released with Python 3.6.

## Model training choices

Due to the un-believability of the input data, an arbitrary cut-off point has been set at 4 hours. Only delays that
lasted less than this duration are used to train the predictive models
for PTV data. Loop data is provided as a lookup CSV and used as is.

## Categories of Delays

These categories were identified in the data manipulated for training
provided, they need to be provided for PTVPredictor, however for
LoopPredictor they are extracted from the CSV file, which may contain
more as is case in the example `data/italy_stats.csv`.

```python3
MESSAGE_CATEGORIES = ["blocked",
                      "slow traffic",
                      "queuing traffic",
                      "roadworks",
                      "stationary traffic"]
```

## Example Usage

There are three cases:

* PTVPredictor
* LoopPredictor
* ComboPredictor

They're all used in a similar manner, the `ComboPredictor` is most
likely to be the best choice for the end user. It is demonstrated
below.

### ComboPredictor Example

```python
"""Exampl    """Example usage"""
import os
from functools import partial

from predictor import ComboPredictor
from process_input import generate_input_rows, categorise
from create_output import OutputClient

# Sample file containing records for predictions
INPUT_FILENAME = os.path.join("data", "ptv_events_data_sample.csv")

""" Using the ComboPredictor """

PTV_TRAINING_FILENAME = os.path.join("data", "incidents_stats.csv")
LOOP_TRAINING_FILENAME = os.path.join("data", "italy_stats.csv")

predictor = ComboPredictor(PTV_TRAINING_FILENAME, LOOP_TRAINING_FILENAME)
output = OutputClient(filename="output.csv")

# Categorise the data
categorised_data = map(partial(categorise, predictor.categories),
						generate_input_rows(INPUT_FILENAME))
# Generate predictions
predictions = map(predictor.predict, categorised_data)

# Create output file
output.write(predictions)e usage"""
```


**Example Output**

Example of output is included as `output.csv`, obtained by running `pipenv run "python main.py"`, which contains
example usage.

## Discussion of developer choices

The program has been designed to use lazy evaluation to the extent
possible in Python3 so as to be more memory efficient for large files.
This choice has led to the functional style in the example usage as `map`
returns a generator. For the expected use-case of CSV to CSV, lazy
evaluation can be used throughout. However, should JSON be required then
the evaluation will be forced immediately prior to writing out as per
the requirement of the JSON standard library.

The program is heavily dependant on pre-made data files. The formats of
these are not checked, it is assumed they will be provided in a
consistent format. Should a Predictor class be created without a path to
such a file an error will be raised. Examples of these files are in the
`data` directory.

Incoming PTV data is not checked, it is assumed to be well formatted and
correct. Should any of the columns in the output format be missing, an
IndexError will be raised. The Predictor class predefines these output
columns for the sake of consistency. PTV data is categorised by looking
for the string of the category in the `incident_message` field of the
PTV record.

Currently the LoopPredictor returns "NA" for expected duration as this
information is not included in the predefined data file.

## Evaluation

Evaluation of the accuracy of this predictor has not been attempted yet
due to not having access to a set of PTV records that are appropriate
for use with the manipulated training data.
